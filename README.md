# Singularity Recipes

A collection of singularity recipes for building singularity images. Examples
for executing these images on midway are included. 

## Build instructions

From local resource where sudo privileges are possible: 
```bash
sudo singularity build <sing-img-filename>.simg <sing-def-filename>.def
```
From midway as root: 
```bash
/software/singularity-3.1.1-el6-x86_64/bin/singularity build tf-gpu-1.13.1_keras-2.2.4.sif tf-gpu-1.13.1_keras-2.2.4
```
